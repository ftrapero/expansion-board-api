from pymodbus.client.sync import ModbusSerialClient as ModbusClient
client = ModbusClient(method = 'rtu', port='/dev/ttyUSB0', stopbits=1, bytesize = 8, parity='N', baudrate = 9600, timeout=0.3)
connection = client.connect()
print("Connection:",connection)
result = client.read_input_registers(0, 2,unit=1)
temperature=result.registers[0]/10.0
print("Temperature:",temperature)
humidity=result.registers[1]/10.0
print("Humidity:",humidity)