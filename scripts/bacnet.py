#!/usr/bin/env python
# -*- coding: utf-8 -*-

#import CRC16
import serial
import time 
import struct
import datetime
import argparse
import bacpypes

from bacpypes.comm import Client, Server, bind
from bacpypes.local.device import LocalDeviceObject
from bacpypes.app import BIPSimpleApplication


from misty.mstplib import MSTPSimpleApplication


class BacnetClientApplication(MSTPSimpleApplication):


    def __init__(self):
         # make a device object
        ini = {
            'objectidentifier' : 51191
        }
        
        mstp_args = {
            '_address': 1,
            '_interface': "/dev/ttyUSB0",
            '_max_masters': 127,
            '_baudrate': 19200,
            '_maxinfo': 1,
            'vendorIdentifier': 525,
            'ini' : ini,
            'objectIdentifier' : 51191
        }
        this_device = LocalDeviceObject(**mstp_args)
        
        
        
if __name__ == "__main__":
    bacnet = BacnetClientApplication()