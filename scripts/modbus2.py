def foo():
    import serial
    import time
    import codecs

    ser = serial.Serial(
        port='/dev/ttymxc4',
        baudrate=9600,
        parity=serial.PARITY_NONE,
        stopbits=serial.STOPBITS_ONE,
        bytesize=serial.EIGHTBITS,
        timeout=0)

    ByteStringToSend = "\x01\x04\x00\x00\x00\x03\xb0\x0b"
    ser.write(ByteStringToSend)
    time.sleep(0.4)

    cc = str(ser.readline())
    hex = codecs.encode(cc, "hex")

    temperatura_hex = (hex[6:10])
    temperatura = int(temperatura_hex, 16) / float(10)
    # humitat_hex=(hex[10:14])
    # humitat = int(humitat_hex, 16)/float(10)
    # Retorna la temperatura
    print(temperatura)

    # Retorna la humitat
    # print(humitat)

    ser.close()
    return temperatura


foo()
