import asyncio
import logging
from typing import Optional

import aiohttp
import sys
import time

log = logging.getLogger('expansion-board-updater')
log.addHandler(logging.StreamHandler(sys.stdout))
log.setLevel(logging.INFO)


async def main():
    modbus_item_name = 'ALFRED_DUMMY7_DUMMY_TEMPERATURE_SENSOR'
    bacnet_item_name = 'ALFRED_DUMMY6_DUMMY_TEMPERATURE_SENSOR'
    modbus_switch_item_name = 'ALFRED_DUMMY9_DUMMY_SWITCH_Switch'
    bacnet_switch_item_name = 'ALFRED_DUMMY8_DUMMY_SWITCH_Switch'

    while True:
        is_modbus_enabled = await is_switch_enabled(modbus_switch_item_name)
        is_bacnet_enabled = await is_switch_enabled(bacnet_switch_item_name)

        if is_modbus_enabled and is_bacnet_enabled:
            log.warning("Either modbus or BACnet can be enabled. Only modbus will be used")

        if not is_modbus_enabled and not is_bacnet_enabled:
            log.error("Both modbus and BACnet are disabled")

        if is_modbus_enabled:
            log.info("Getting modbus temperature")
            temperature: Optional[float] = await get_modbus_temperature()

            if temperature is not None:
                await set_item_value(modbus_item_name, str(temperature))
            else:
                log.error("Modbus item %s was not updated", modbus_item_name)

        if is_bacnet_enabled and not is_modbus_enabled:
            log.info("Getting BACnet temperature")
            temperature: Optional[float] = await get_bacnet_temperature()

            if temperature is not None:
                await set_item_value(bacnet_item_name, str(temperature))
            else:
                log.error("BACnet item %s was not updated", bacnet_item_name)

        time.sleep(5.0)


async def get_modbus_temperature() -> Optional[float]:
    modbus_url = 'http://192.168.7.1:5000/modbus/thermostat'
    json_response = None

    try:
        async with aiohttp.ClientSession(timeout=aiohttp.ClientTimeout(total=20.0)) as session:
            async with session.get(modbus_url) as resp:
                json_response = await resp.json()
                temperature = float(json_response['temperature'])
                return temperature
    except Exception as e:
        log.error("cannot retrieve temperature: %s: %s", json_response, e)

    return None


async def get_bacnet_temperature() -> Optional[float]:
    bacnet_url = 'http://192.168.7.1:5000/bacnet/energy_meter'
    json_response = None

    try:
        async with aiohttp.ClientSession(timeout=aiohttp.ClientTimeout(total=30.0)) as session:
            async with session.get(bacnet_url) as resp:
                json_response = await resp.json()
                temperature = float(json_response['consumption'].replace("\n", ""))
                return temperature
    except Exception as e:
        log.error("cannot retrieve temperature: %s: %s", json_response, e)

    return None


async def set_item_value(item_name: str, value: str) -> None:
    item_url = f'http://localhost:8080/rest/items/{item_name}/state'

    try:
        headers = {
            "Content-Type": "text/plain",
            'Accept': 'application/json'
        }
        async with aiohttp.ClientSession(
                headers=headers,
                timeout=aiohttp.ClientTimeout(total=10.0)
        ) as session:
            async with session.put(item_url, data=value) as resp:
                if resp.status != 202:
                    log.error("Error while updating the status")
                else:
                    log.info("Temperature updated to %s", value)
    except Exception as e:
        log.error("cannot update temperature: %s: %s", value, e)


async def is_switch_enabled(item_name: str) -> bool:
    item_url = f'http://localhost:8080/rest/items/{item_name}/state'

    try:
        async with aiohttp.ClientSession(
                timeout=aiohttp.ClientTimeout(total=10.0)
        ) as session:
            async with session.get(item_url) as resp:
                value = await resp.text()
                if resp.status != 200:
                    log.error("Error while checking the status")
                    return False
                return value == 'ON'
    except Exception as e:
        log.error("cannot check status: %s: %s", item_name, e)


if __name__ == "__main__":
    asyncio.run(main())
