import os
import subprocess


def main():
    filepath = '/home/root/expansion-board-api/scripts/bacnet-values.txt'
    lock_filepath = '/home/root/expansion-board-api/scripts/bacnet-values.lock'

    if os.path.exists(lock_filepath):
        print('Lock file exists. Exiting.')
        return
    else:
        with open(lock_filepath, 'w') as f:
            f.write('')

    try:
        os.remove(filepath)
    except OSError:
        pass

    value = None
    env_vars = {"BACNET_IFACE": "/dev/ttymxc4", "BACNET_MSTP_BAUD": "9600"}
    command = ["/home/root/expansion-board-api/scripts/bacpoll", "783001", "0", "0", "--print-seconds", "1"]
    process = subprocess.Popen(command, env=env_vars, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    try:
        process.wait(timeout=20)
        with open(filepath, 'r') as f:
            value = f.read()
        os.remove(filepath)
    except subprocess.TimeoutExpired:
        process.kill()

    try:
        os.remove(lock_filepath)
    except OSError:
        pass

    print(value)


if __name__ == '__main__':
    main()
