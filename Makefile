setup-venv:
	python3 -m venv venv
	./venv/bin/pip3 install -r requirements.txt

run:
	./venv/bin/gunicorn --bind 0.0.0.0:5000 src.app:app
