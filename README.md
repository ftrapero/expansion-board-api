## Install the API

It is required python3.7 or later.

Once you download the project, setup the python virtual environment with:

```
make setup-venv
```

Then, run the application with:

```
make run
```

To test the service is running, you can use curl:

```
curl -X GET http://localhost:5000/status
```

It should return:

```
{"status":"OK"}
```

# Modbus

Get the temperature:

```
curl -X GET http://localhost:5000/modbus/thermostat
```


# BacNET

Get the energy consumption:

```
curl -X GET http://localhost:5000/bacnet/energy_meter
```

# Thread

Get the lightbulb status:

```
curl -X GET http://localhost:5000/thread/lightbulb
```

Turn on/off the lightbulb:

```
curl -X POST http://localhost:5000/thread/lightbulb -H "Content-Type: application/json" -d '{"status": "ON"}'
```