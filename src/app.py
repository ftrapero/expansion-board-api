import os

from flask import Flask, request

app = Flask(__name__)

LOCK_FILEPATH = '/home/root/expansion-board-api/scripts/bacnet-values.lock'
try:
    os.remove(LOCK_FILEPATH)
except OSError:
    pass


@app.route("/status", methods=['GET'])
def status():
    return {
        "status": "OK"
    }


@app.route("/modbus/thermostat", methods=['GET'])
def modbus_thermostat_info():
    import subprocess
    python2_command = "python2 scripts/modbus2.py"  # launch your python2 script
    process = subprocess.Popen(python2_command.split(), stdout=subprocess.PIPE)
    output, error = process.communicate()  # receive output from the python2 scr

    value = output.decode('ascii')
    value2 = value.strip()
    return {
        "temperature": value2  # �°C
    }


@app.route("/bacnet/energy_meter", methods=['GET'])
def bacnet_energy_meter_info():
    import subprocess
    python3_command = "python3 scripts/bacnet-app.py"  # launch your python script
    process = subprocess.Popen(python3_command.split(), stdout=subprocess.PIPE)
    output, error = process.communicate()  # receive output from the python2 scr

    value3 = output.decode('ascii')
    return {
        "consumption": value3  # kWh
    }


@app.route("/thread/lightbulb", methods=['GET', 'POST'])
def thread_lightbulb_info():
    if request.method == 'GET':
        return {
            "status": "ON"
        }
    else:
        print(request.json['status'])
        return {}


if __name__ == "__main__":
    app.run(host="0.0.0.0")
